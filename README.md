# 2UP AGENCY recruitment project

## info

`$ npm -v
v6.12.0`

`$ node -v
v12.13.0`

## 1st run

`npm install`

## compile and run

```bash
npm run webpack
npm run start
```

## environment

React + typescript & webpack + babel & eslint & prettier & mobx

Aleksander Nowicki
