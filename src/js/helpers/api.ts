import axios from "axios";

const API_URL = "https://jsonplaceholder.typicode.com";
axios.defaults.baseURL = API_URL;

export const getPosts = () => axios.get("/posts")
export const getPost = (id: number) => axios.get(`/posts/${id}`)
export const getPostComments = (id: number) => axios.get(`/posts/${id}/comments`)