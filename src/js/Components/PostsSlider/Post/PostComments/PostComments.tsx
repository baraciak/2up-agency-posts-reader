import React, { useState } from 'react'
import { Comment } from '../../PostsSlider'
import { Collapse } from 'react-collapse';

interface Props {
    comments: Comment[]
}

const PostComments = ({ comments }: Props) => {
    const [showMore, setShowMore] = useState(false);

    const renderComments = (comment: Comment) => (
        <div key={comment?.id} className="slick-slide__content--comment">
            <div className="slick-slide__content--comment-email">{comment?.email}</div>
            <div className="slick-slide__content--comment-email">{comment?.name}</div>
            <p className="text-muted pb-3 text-underline">{comment?.body}</p>
        </div>
    )

    return (
        comments ? (
            <>
                {comments?.map((comment: Comment, index: number) => {
                    if(index < 2){
                        return renderComments(comment)
                    }
                    return null
                })}  
                <Collapse isOpened={showMore}>
                {comments?.map((comment: Comment, index: number) => {
                    if(index >= 2){
                        return renderComments(comment)
                    }
                    return null
                })}  
                </Collapse>
                <div className="clickable show-more-comments color-primary"
                    onClick={() => setShowMore(!showMore)}
                >
                    <i className={`me-2 ${showMore ? `bi-chevron-up` : `bi-chevron-down`}`} />
                    {showMore ? "Show Less" : "Show More"}
                </div>
            </>
        )
        : (
            <div className='d-flex justify-content-center align-items-center w-100 h-100'>
                <div className="spinner-border text-primary" role="status" />
            </div>
        )
    )
}

export default PostComments
