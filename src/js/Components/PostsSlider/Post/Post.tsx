import React from 'react'
import PostComments from './PostComments/PostComments'
import { Comment } from '../PostsSlider'

interface Props {
    title: string;
    userId: number;
    body: string;
    comments: Comment[];
}

const Post = ({
    title,
    userId,
    body,
    comments
}: Props) => {
  return (
    <>
        <div className="slick-slide__content--title">{title}</div>
        <small className="text-muted">@Author{userId}</small>
        <div className="slick-slide__content--body">{body}</div>
        <div className="slick-slide__content--comments">
            <div className="mb-3 text-underline color-primary">Comments</div>
            <PostComments comments={comments} />
        </div>
    </>
  )
}

export default Post
