import _ from 'lodash';
import { useObserver } from 'mobx-react';
import React, { useEffect, useRef, useState } from 'react';
import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import { getPostComments, getPosts } from '../../helpers/api';
import { usePostsStore } from '../../store/PostsContext';
import Post from './Post/Post';
import './PostsSlider.scss';

interface Props {

}

interface Post {
    body: string;
    id: number;
    title: string;
    userId: number;
    comments: Comment[]
}

export interface Comment {
    body: string;
    email: string;
    id: number;
    name: string;
    postId: number;
}

const PostsSlider = ({}: Props) => {
    const [currentSlideIndex, setCurrentSlideIndex] = useState<number>(0)
    let sliderRef = useRef<any>()
    const postsStore: any = usePostsStore();

    useEffect(() => {
        handleGetPosts()
    }, [])

    useEffect(() => {
        if(postsStore?.posts?.length && postsStore?.posts[currentSlideIndex] && postsStore?.posts[currentSlideIndex + 1]){
            !postsStore?.posts[currentSlideIndex]?.comments && handleGetPostComments(currentSlideIndex)
            !postsStore?.posts[currentSlideIndex + 1]?.comments && handleGetPostComments(currentSlideIndex + 1)
        }
    }, [currentSlideIndex, postsStore?.posts?.length])

    const handleGetPosts = async () => {
        try {
            const response: any = await getPosts();
            if(response?.data?.length){
                postsStore?.setPosts(response?.data)
            }
        } catch(e: any) {
            console.log("Error")
        }
    }

    const handleGetPostComments = async (postIndex: number) => {
        // console.log(["handleGetPostComments", postIndex])
        try {
            const response: any = await getPostComments(postsStore?.posts[postIndex]?.id);
            postsStore?.setPostComments(postIndex, response?.data)
        } catch(e: any) {
            console.log("Error")
        }
    }

    const handleSwipeChange = (direction: string) => {
        if(!sliderRef) return;
        if(direction === 'right' && currentSlideIndex === 0) return;
        if(direction === 'left' && currentSlideIndex >= postsStore?.posts?.length - 1) return;

        setCurrentSlideIndex(direction === 'right' ? currentSlideIndex - 1 : currentSlideIndex + 1)
    }
    
    const handleSlideChange = (direction: string) => {
        if(!sliderRef) return;
        if(direction === 'right' && currentSlideIndex === 0) return;
        if(direction === 'left' && currentSlideIndex >= postsStore?.posts?.length - 1) return;
        
        // @ts-ignore
        direction === 'right' ? sliderRef.slickPrev() : sliderRef.slickNext()
        setCurrentSlideIndex(direction === 'right' ? currentSlideIndex - 1 : currentSlideIndex + 1)
    }

    const settings = {
        // dots: true,
        speed: 200,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        onSwipe: handleSwipeChange
    };

    return useObserver(() => (
        <>
            <div className="custom-navbar">
                <div className='arrows-container'>
                    <i onClick={_.debounce(() => handleSlideChange('right'), 200)}
                        className={`bi-arrow-left-circle ${currentSlideIndex !== 0 
                            ? "clickable color-primary"
                            : "text-muted"
                        }`}
                    />
                    <i onClick={_.debounce(() => handleSlideChange('left'), 200)}
                        className={`bi-arrow-right-circle ${!(currentSlideIndex >= postsStore?.posts?.length - 1) 
                            ? "clickable color-primary"
                            : "text-muted"
                        }`}
                    />
                </div>
            </div>
            {/* @ts-ignore */}
            <Slider ref={slider => sliderRef = slider} {...settings}>
                {postsStore?.posts?.map((post: Post) => (
                    <div key={post?.id} onDragStart={(e) => console.log(e)}>
                        <div className="slick-slide__content">
                            <small className="text-muted">Article {currentSlideIndex + 1}/{postsStore?.posts?.length}</small>
                            <Post 
                                title={post?.title}
                                userId={post?.userId}
                                body={post?.body}
                                comments={post?.comments}
                            />
                        </div>
                    </div>
                ))}
            </Slider>
        </>
    ))
}

export default PostsSlider
