import React from 'react'
import PostsSlider from './Components/PostsSlider/PostsSlider';
import { PostsProvider } from './store/PostsContext';

interface Props {
    message?: string;
};

const App = ({}: Props) => {
    return (
        <div className="app-container">
            <PostsProvider>
                <PostsSlider />
            </PostsProvider>
        </div>
    )
}

export default App