import React from 'react'
import { createPostsStore } from './PostsStore/PostsStore'
import { useLocalStore } from 'mobx-react'

const PostsContext = React.createContext(null)

export const PostsProvider = ({children}: any) => {
    const postsStore: any = useLocalStore(createPostsStore)

    return (
        <PostsContext.Provider value={postsStore}>
            {children}
        </PostsContext.Provider>
    )
}

export const usePostsStore = () => React.useContext(PostsContext)