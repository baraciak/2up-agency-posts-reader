export function createPostsStore(){
    return {
        posts: [],
        setPosts(posts: any){
            this.posts = posts;
        },
        setPostComments(postIndex: number, comments: any[]){
            const postsCopy: any = [...this.posts];
            postsCopy[postIndex].comments = comments;
            this.posts = postsCopy;
        }
    }
}